<?php
include('database.php');
session_start();
if(!isset($_SESSION["sess_id"])) {
  header("Location:index.php");
} else {
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Application</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">APP</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Application</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="img/icon.png" class="user-image" alt="User Image">
              <span class="hidden-xs">User</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="img/icon.png" class="img-circle" alt="User Image">

                <p>
                  User
                  <!-- <small>Member since Nov. 2012</small> -->
                </p>
              </li>
              <li class="user-footer">
                <!-- <div class="pull-left">
                  <a href="profile.php" class="btn btn-default btn-flat">Profile</a>
                </div> -->
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="img/icon.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>User</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="table.php"><i class="fa fa-users"></i> <span>Employees</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Employees
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Employees</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Employee ID</th>
                  <th>First Name</th>
                  <th>Middle Name</th>
                  <th>Last Name</th>
                  <th>Department</th>
                  <th>Hire Date</th>
                  <th>Image</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  $record_per_page=10;
                  if(isset($_GET['page'])){
                    $page=$_GET['page'];
                  } else {
                    $page=1;
                  }
                  $start_from=($page-1)*$record_per_page;

                    $sql="SELECT a.emp_id empid, a.first_name firstname, a.middle_name middlename, a.last_name lastname, b.name department, a.hire_date hiredate, a.profile_pic picture FROM employees a INNER JOIN departments b on a.department_id=b.department_id ORDER BY a.emp_id ASC LIMIT $start_from, $record_per_page";
                   $result=mysqli_query($con,$sql);
                  
                        while ($row=mysqli_fetch_array($result)) {
                          $this[]=$row;  
                      }
                      foreach ($this as $cell) {
                        echo "<tr>";
                      echo "<td>". $cell['empid'] . "</td>"; 
                      echo "<td>". $cell['firstname'] . "</td>";
                      echo "<td>". $cell['middlename'] . "</td>";
                      echo "<td>". $cell['lastname'] . "</td>";
                      echo "<td>". $cell['department'] . "</td>";
                      if (is_null($cell['hiredate'])){
                      echo "<td></td>";
                    } else {
                       echo "<td>". date("F d, Y",strtotime($cell['hiredate'])) . "</td>";
                    }
                    if (file_exists($cell['picture'])) {
                       echo "<td><img src='".$cell['picture']."' height=100 width=100>";
                    } else {
                       echo "<td></td>";
                    }
                      
                      echo "</tr>";
                      }

                      $page_query="SELECT a.emp_id empid, a.first_name firstname, a.middle_name middlename, a.last_name lastname, b.name department, a.hire_date hiredate FROM employees a INNER JOIN departments b on a.department_id=b.department_id ORDER BY a.emp_id ASC";
                      $page_result=mysqli_query($con,$page_query);
                      $total_records=mysqli_num_rows($page_result);
                      $total_pages=ceil($total_records/$record_per_page);



                  ?>
                </tbody>
              </table>
              <?php
               for($i=1;$i<=$total_pages;$i++){
                        echo '<ul class="pagination"><li>
                        <a href="table.php?page='.$i.'">'.$i.'</a></li></ul>';
                      }
               ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Blances Sanchez</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<!-- <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script> -->
</body>
</html>
<?php 
}
?>