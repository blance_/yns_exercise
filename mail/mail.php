<?php
//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once 'vendor/autoload.php';


$mail = new PHPMailer(true);
$reply = new PHPMailer(true);


if (isset($_POST['send'])) {

    //$email_to="blancessanchez30@gmail.com";

    function died($error) {
        echo "Error found: $error " . "<br>";
        die();
    }

    if(empty($_POST['firstname']) || empty($_POST['lastname']) || empty($_POST['email']) || empty($_POST['message']) || empty($_POST['contact']) ) {
        died('Fill in the required fields.');
    }


    $subject=mysql_real_escape_string($_POST['subject']);
    $first_name=mysql_real_escape_string($_POST['firstname']);
    $last_name=mysql_real_escape_string($_POST['lastname']);
    $email_from=mysql_real_escape_string($_POST['email']);
    $contact_no=mysql_real_escape_string($_POST['contact']);
    $message=mysql_real_escape_string($_POST['message']);

    $error_message="";


    $email_message="Form details below.\n\n";

    function clean_string($string) {
        $bad=array("content=-type","bcc:","to:","cc:","href");
        return str_replace($bad,"",$string);
    }

    try {
        $mail->isSMTP();
        $mail->SMTPDebug = 3;
        $mail->Host = 'smtp.gmail.com';  //mailtrap SMTP server
        $mail->SMTPAuth = true;
        $mail->Username = 'testemail.yema@gmail.com';   //username
        $mail->Password = 'th!$happens';   //password
        $mail->SMTPSecure='ssl';
        $mail->Port = 465;                //smtp port

        $mail->setFrom($email_from, $first_name);
        $mail->addAddress($email_from, 'Blance');

        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body    = 'Name:' .$first_name.' '.$last_name .'<br> Email: '.$email_from.'<br> Contact No.: ' .$contact_no.'<br>Message: '.$message;

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $reply->isSMTP();
        $reply->SMTPDebug = 3;
        $reply->Host = 'smtp.gmail.com';  //mailtrap SMTP server
        $reply->SMTPAuth = true;
        $reply->Username = 'testemail.yema@gmail.com';   //username
        $reply->Password = 'th!$happens';   //password
        $reply->SMTPSecure='ssl';
        $reply->Port = 465;                //smtp port

        $reply->setFrom('testemail.yema@gmail.com', $first_name);
        $reply->addAddress('testemail.yema@gmail.com', 'Blance');

        $reply->isHTML(true);

        $reply->Subject = $subject;
        $reply->Body    = 'Thanks for submitting an email. This serves as a confirmation';

        $reply->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        if (!$mail->send() || !$reply->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo "<script>alert('Success! Message sent.')</script>";
            echo "<script>window.open('index.php','_self')</script>";
        }
    } catch (Exception $e) {
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }
} else {
    echo "<script>alert('Error $_POST')</script>";
}