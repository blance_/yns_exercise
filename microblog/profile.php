<?php
include('classes/DB.php');
include('classes/Login.php');
include('classes/Post.php');
include('classes/Pictures.php');

$username="";
$isFollowing=false;
$verified=false;

if(isset($_GET['username'])) {
    if(DB::query('SELECT username FROM users WHERE username=:username', array(':username'=>$_GET['username']))) {

        $username = DB::query('SELECT username FROM users WHERE username=:username', array(':username'=>$_GET['username']))[0]['username'];
        $userid = DB::query('SELECT id FROM users WHERE username=:username', array(':username'=>$_GET['username']))[0]['id'];
        $verified = DB::query('SELECT verified FROM users WHERE username=:username', array(':username'=>$_GET['username']))[0]['verified'];
        $followerid = Login::isLoggedIn();

        if(isset($_POST['follow'])) {
            if($userid != $followerid) {
                if(!DB::query('SELECT follower_id FROM followers WHERE user_id=:userid AND follower_id=:followerid', array(':userid'=>$userid, ':followerid'=>$followerid))){
                    if($followerid==4) {
                        DB::query('UPDATE users SET verified=1 WHERE id=:userid', array(':userid'=>$userid));
                    }
                    DB::query('INSERT INTO followers VALUES (\'\', :userid, :followerid)', array(':userid'=>$userid, ':followerid'=>$followerid));
                } else {
                    echo "Already following";
                }
                $isFollowing=true;
            }
        }
        if(isset($_POST['unfollow'])){
            if($userid != $followerid) {
                if(DB::query('SELECT follower_id FROM followers WHERE user_id=:userid AND follower_id=:followerid', array(':userid'=>$userid, ':followerid'=>$followerid))){
                    if($followerid==4) {
                        DB::query('UPDATE users SET verified=0 WHERE id=:userid', array(':userid'=>$userid));
                    }
                    DB::query('DELETE FROM followers WHERE user_id=:userid AND follower_id=:followerid', array(':userid'=>$userid, ':followerid'=>$followerid));
                } 
                $isFollowing=false;
            }
        }
        if(DB::query('SELECT follower_id FROM followers WHERE user_id=:userid AND follower_id=:followerid', array(':userid'=>$userid, ':followerid'=>$followerid))){
            $isFollowing=true;
        }
        if(isset($_POST['post'])){

            $postPic=$_FILES['postImg']['name'];
            $temp=$_FILES['postImg']['tmp_name'];
            Post::createPost($_POST['postbody'], Login::isLoggedIn(), $userid, $postPic, $temp);
        }

        if(isset($_GET['postid']) && !isset($_POST['deletepost'])){
            Post::likesPost($_GET['postid'], $followerid);
        }
       
        $posts = Post::displayPost($userid, $username, $followerid);
    } else {
        die('User not found');
    }

    if(isset($_POST['deletepost'])) {
        if(DB::query('SELECT id FROM posts WHERE id=:postid AND user_id=:userid', array(':postid'=>$_GET['postid'], ':userid'=>$followerid))) {
            DB::query('DELETE FROM posts WHERE id=:postid and user_id=:userid', array(':postid'=>$_GET['postid'], ':userid'=>$followerid));
            DB::query('DELETE FROM post_likes WHERE post_id=:postid', array(':postid'=>$_GET['postid']));
            echo 'Post deleted';
        }
    }
}
?>
<h2><?php echo $username ?>'s Profile - <?php if ($verified) { echo ' Verified'; }?></h2>
<?php echo "<img src='".Pictures::displayProfilePic($userid)."' height='100' width='100'>"?>
<form action="profile.php?username=<?php echo $username ?>" method="post">
    <?php 
    if($userid != $followerid) {
        if($isFollowing) {
            echo '<input type="submit" name="unfollow" value="Unfollow">';
        } else {
            echo '<input type="submit" name="follow" value="Follow">';
        }
    }
    ?>
</form>

<form action="profile.php?username=<?php echo $username ?>" method="post" enctype="multipart/form-data">
    <textarea rows="8" col="80" name="postbody"></textarea><p/>
    Upload picture:
    <input type="file" name="postImg">
    <input type="submit" name="post" value="Post">
</form>

<div class="posts">
    <?php echo $posts;?>
</div>