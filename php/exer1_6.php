<!DOCTYPE html>
<html>
<head>
    <title>User Information</title>
</head>
<body>
    <form action="view_info.php" method="post" enctype="multipart/form-data">
        <p> Enter username: </p>
        <input type="text" name="username" required>

        <p> Enter password: </p>
        <input type="password" name="password" required>

        <p> Enter first name: </p>
        <input type="text" name="fname" required/>

        <p> Enter middle initial: </p>
        <input type="text" name="minitial" required/>

        <p> Enter last name: </p>
        <input type="text" name="lname"  required/>

        <p> Enter email:</p>
        <input type="text" name="email" required/>

        <p> Enter address:</p>
        <input type="text" name="address" required/>

        <p> Zip code:</p>  
        <input type="number" name="zipcode" required/>

        <p> Select image to upload: </p>
        <input type="file" name="fileUp" id="fileUp" required>
        <br><br>

        <input type="submit" name="submit" value="Submit"/> 
    </form>
    <a href="exer1_9.php">Show table data</a>
</body>
</html>