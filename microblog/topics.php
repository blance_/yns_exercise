<?php
include('classes/DB.php');
include('classes/Login.php');
include('classes/Post.php');
include('classes/Pictures.php');

if(isset($_GET['topic'])) {
    if(DB::query('SELECT topics FROM posts WHERE find_in_set(:topic, topics)', array(':topic'=>$_GET['topic']))) {
        $posts = DB::query('SELECT * FROM posts WHERE find_in_set(:topic, topics)', array(':topic'=>$_GET['topic']));
        foreach ($posts as $post) {
            echo $post['body'].'<br>';
        }
    }
    
}
?>