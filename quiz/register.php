<?php 
  require_once('include/database.php');
  if(isset($_POST) & !empty($_POST)){
    $fname = $database->sanitize($_POST['firstname']);
    $lname = $database->sanitize($_POST['lastname']);
    $username = $database->sanitize($_POST['username']);
    $password = $database->sanitize(sha1($_POST['password']));
    $profile_pic ="profile_pic/".$_FILES['image']['name'];

    $res = $database->register($fname, $lname, $username, $password, $profile_pic);
    if($res) {
      echo "<script>
      alert('Successfully created a new account.');
      window.location.href='index.php';
      </script>";
    } else {
      // echo "<script>alert('Failed to register.');</script>";
    }
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quiz | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="dist/css/style.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="#">Quiz</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new account</p>

    <form method="post" enctype="multipart/form-data" name="regForm">
      <div class="form-group has-feedback">
        <input type="text" name="firstname" class="form-control" placeholder="First name" required>
        <span class="errorMsg"></span>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="lastname" class="form-control" placeholder="Last name" required>
        <span class="errorMsg"></span>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Username" required>
        <span class="errorMsg"></span>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password" required>
       <span class="errorMsg"></span>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <label class="custom-file">Upload Picture:
      <input type="file" id="file" name="image" class="custom-file-input" required>
     <span class="errorMsg"></span>
      <span class="custom-file-control"></span>
    </label>
      <div class="row">
        <div class="col-xs-8">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="register" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <a href="index.php" class="text-center">I already have an account</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>


<script src="dist/js/validation.js"></script>
<!--   $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });

  var x=document.forms["regForm"]["firstname"].value;
    if(x=="") {
        alert("Input must be complete");
        return false;
    }
</script> -->
</body>
</html>
