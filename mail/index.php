
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Mail Form</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="../../index2.html" class="navbar-brand"><b>Mail</b>Form</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
        <form class="form-horizontal" action="mail.php" method="post">
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Subject *</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" name="subject" placeholder="Subject">
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Email Address *</label>

            <div class="col-sm-10">
              <input type="email" class="form-control" name="email" placeholder="Email Address">
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">First Name *</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" name="firstname" placeholder="First Name" required>
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Last Name *</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" name="lastname" placeholder="Last Name" required>
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Contact Number</label>

            <div class="col-sm-10">
              <input type="text" class="form-control" name="contact" placeholder="Contact Number" required>
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Message *</label>

            <div class="col-sm-10">
              <textarea class="form-control" name="message" placeholder="Input your message here..."></textarea>
            </div>
          </div>
          <div class="col-sm-10">
          <p style="color:red;">* Fill in the required fields</p>
        </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" name="send" class="btn btn-success"> <i class="fa fa-send"></i>&nbspSend</button>
            </div>
          </div>
        </form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <strong>Copyright &copy; 2018 <a href="#">Blances Sanchez</a>.</strong> All rights
    reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
