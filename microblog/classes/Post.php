<?php 
class Post {
    public static function createPost($postbody, $loggedinUser, $profileUserId, $postPic, $tmp) {
       
        if(strlen($postbody)>140 || strlen($postbody)<1){
            die('Incorrect length');
        } 

        $topics = self::getTopics($postbody);
        if($loggedinUser==$profileUserId) {
            if(isset($_GET['postPic'])){
                if(!file_exists('pictures/')) {
                    mkdir('pictures/', 077, true);
                    }
        
                    $dir = "pictures/";
                    $picture = $dir.$postPic;
                    $pictureTarget = $dir.basename($picture);
        
                    DB::query('INSERT INTO posts(body, user_id, post_pic, topics) VALUES (:postbody, :userid, :post_pic, :topics)', array(':postbody'=>$postbody, ':userid'=>$profileUserId, ':post_pic'=>$picture, ':topics'=>$topics));
            } else {
                DB::query('INSERT INTO posts(body, user_id, topics) VALUES (:postbody, :userid, :topics)', array(':postbody'=>$postbody, ':userid'=>$profileUserId, ':topics'=>$topics));
            }
        } else {
            echo "Incorrect user";
        }
    }

    public static function likesPost($postid, $likerId) {
        if(!DB::query('SELECT user_id FROM post_likes WHERE post_id=:postid AND user_id=:userid', array(':postid'=>$postid, ':userid'=>$likerId))){
            DB::query('UPDATE posts SET likes=likes+1 WHERE id=:postid', array(':postid'=>$postid));
            DB::query('INSERT INTO post_likes VALUES (\'\', :postid, :userid)', array(':postid'=>$postid, ':userid'=>$likerId));
        } else {
            DB::query('UPDATE posts SET likes=likes-1 WHERE id=:postid', array(':postid'=>$postid));
            DB::query('DELETE FROM post_likes WHERE post_id=:postid AND user_id=:userid', array(':postid'=>$postid, ':userid'=>$likerId));
        }
    }

    public static function link_add($text) {
        $text=explode(" ", $text);
        $newstring="";
        foreach($text as $word) {
            if(substr($word, 0, 1)=="@") {
                $newstring .="<a href='profile.php?username=".substr($word, 1)."'>".htmlspecialchars($word). "</a>";
            } else if(substr($word, 0, 1)=="#") {
                $newstring .="<a href='topics.php?topic=".substr($word, 1)."'>".htmlspecialchars($word). "</a>";
            } else {
                $newstring .=htmlspecialchars($word)." ";
            }
        }
        return $newstring;
    }

    public static function getTopics($text) {
        $text=explode(" ", $text);
        $topics="";
        foreach($text as $word) {
            if(substr($word, 0, 1)=="#") {
                $topics .= substr($word, 1).",";
            } 
        }
        return $topics;
    }

    public static function displayPost($userid, $username, $loginUserId) {
        $dbposts=DB::query('SELECT * FROM posts WHERE user_id=:userid ORDER BY id DESC', array(':userid'=>$userid));
        $posts="";
        foreach($dbposts as $p) {
            if(file_exists($p['post_pic'])) {
                if(!DB::query('SELECT post_id FROM post_likes WHERE post_id=:postid AND user_id=:userid', array(':postid'=>$p['id'], ':userid'=>$loginUserId))){
                    $posts .= self::link_add($p['body']) ."<img src='".$p['post_pic']."' height='100' width='100'> 
                    <form action='profile.php?username=$username&postid=".$p['id']."' method='post'>
                        <input type='submit' name='like' value='Like'>
                        <span>".$p['likes']." likes</span>";
                    if($userid == $loginUserId) {
                        $posts.= "<input type='submit' name='deletepost' value='X'>";
                    }

                $posts .= "</form><hr><br>";
                } else {
                    $posts .= self::link_add($p['body']) . "<img src='".$p['post_pic']."' height='100' width='100'> 
                    <form action='profile.php?username=$username&postid=".$p['id']."' method='post'>
                        <input type='submit' name='unlike' value='Unlike'>
                        <span>".$p['likes']." likes</span>
                        ";
                    if($userid == $loginUserId) {
                        $posts.= "<input type='submit' name='deletepost' value='X'>";
                    }

                $posts .= "</form><hr><br>";
                }
            } else {
                if(!DB::query('SELECT post_id FROM post_likes WHERE post_id=:postid AND user_id=:userid', array(':postid'=>$p['id'], ':userid'=>$loginUserId))){
                    $posts .= self::link_add($p['body']). 
                    "<form action='profile.php?username=$username&postid=".$p['id']."' method='post'>
                        <input type='submit' name='like' value='Like'>
                        <span>".$p['likes']." likes</span>";
                        if($userid == $loginUserId) {
                            $posts.= "<input type='submit' name='deletepost' value='X'>";
                        }
    
                    $posts .= "</form><hr><br>";
                    
                } else {
                    $posts .= self::link_add($p['body']) . 
                    "<form action='profile.php?username=$username&postid=".$p['id']."' method='post'>
                        <input type='submit' name='unlike' value='Unlike'>
                        <span>".$p['likes']." likes</span>";
                        if($userid == $loginUserId) {
                            $posts.= "<input type='submit' name='deletepost' value='X'>";
                        }
    
                    $posts .= "</form><hr><br>";
                    
                }
            }
        }
        return $posts;
    }
}
