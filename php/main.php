<!DOCTYPE html>
<html>
<head>
    <?php 
        session_start();
        if(!isset($_SESSION["userid"])) {
            header("Location:../index.php");
        } else { 
    ?>
    <title>Exercises</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<table>
    <tr>
        <th>Exer No.</th>
        <th>Link</th>
    </tr>

    <tr>
        <td>1-1</td>
        <td><a href="exer1_1.php">Hello World</a></td>
    </tr>

    <tr>
        <td>1-2</td>
        <td><a href="exer1_2.php">Four Basic Operations of Arithmetic</a></td>
    </tr>

    <tr>
        <td>1-3</td>
        <td><a href="exer1_3.php">Greatest Common Divisor</a></td>
    </tr>

    <tr>
        <td>1-4</td>
        <td><a href="exer1_4.php">FizzBuzz Problem</a></td>
    </tr>

    <tr>
        <td>1-5</td>
        <td><a href="exer1_5.php">Input Date</a></td>
    </tr>

    <tr>
        <td>1-6, 1-7, 1-8, 1-10</td>
        <td><a href="exer1_6.php">User Information</a></td>
    </tr>

    <tr>
        <td>1-9, 1-11</td>
        <td><a href="exer1_9.php">Show User Information</a></td>
        <!-- with pagination
        <td><a href="exer1_9_bu.php">Show User Information</a></td>-->
    </tr>
    <tr>
        <td>1-13</td>
        <td><a href="../index.php">Login Page</a></td>
    </tr>

</table>
</body>

<?php
    }
    session_destroy();
    session_unset();
 ?> 
</html>