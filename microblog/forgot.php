<!DOCTYPE html>
 <html>
  <head>
   <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  </head>
  <body>
<?php
include('classes/DB.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once 'vendor/autoload.php';


$mail = new PHPMailer(true);

if (isset($_POST['resetpassword'])) {

    $cstrong=True;
    $token = bin2hex(openssl_random_pseudo_bytes(64, $cstrong));
    $email=$_POST['email'];
    $user_id = DB::query('SELECT id FROM users WHERE email=:email', array(':email'=>$email))[0]['id'];
    DB::query('INSERT INTO password_tokens VALUES (\'\', :token, :user_id)', array(':token'=>sha1($token), ':user_id'=>$user_id));


    if(empty($_POST['email'])) {
        echo 
        '<script>
        swal({
            title: "Error!",
            text: "Fill in the required fields",
            type: "warning"
        }, function() {
            window.location = "forgotpassword.php";
        });

    </script>';
    }

    $email=mysql_real_escape_string($_POST['email']);

    $error_message="";

    $email_message="Form details below.\n\n";

    function clean_string($string) {
        $bad=array("content=-type","bcc:","to:","cc:","href");
        return str_replace($bad,"",$string);
    }

    try {
        $mail->isSMTP();
        $mail->SMTPDebug = 2;
        $mail->Host = 'smtp.gmail.com';  
        $mail->SMTPAuth = true;
        $mail->Username = 'testemail.yema@gmail.com';   
        $mail->Password = 'th!$happens';   
        $mail->SMTPSecure='ssl';
        $mail->Port = 465;              

        $mail->setFrom($email,'MicroBlog');
        $mail->addAddress($email, 'Password Reset');

        $mail->isHTML(true);

        $mail->Subject = 'MicroBlog Password Reset';
        $mail->Body    = 'Password reset: <br> http://microblog.com/changepassword.php?token='.$token;

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

       
        if (!$mail->send()) {
            echo 
            '<script>
            swal({
                title: "Message could not be sent!",
                text: "'.$mail->ErrorInfo.'",
                type: "success"
            }, function() {
                window.location = "forgotpassword.php";
            });
            </script>';
        } else {
            echo 
            '<script>
            swal({
                title: "Success!",
                text: "Password reset sent",
                type: "success"
            }, function() {
                window.location = "forgotpassword.php";
            });

        </script>';
        }
    } catch (Exception $e) {
        echo 
        '<script>
        swal({
            title: "Mailer error!",
            text: "'.$mail->ErrorInfo.'",
            type: "success"
        }, function() {
            window.location = "forgotpassword.php";
        });
    </script>';
    }
} else {
    echo 
        '<script>
        swal({
            title: "Error!",
            text: "Error POST",
            type: "warning"
        }, function() {
            window.location = "forgotpassword.php";
        });

    </script>';
}
?>
</body>
