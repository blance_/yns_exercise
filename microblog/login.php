<!DOCTYPE html>
 <html>
  <head>
   <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  </head>
  <body>
<?php 
include('classes/DB.php');

if (isset($_POST['login'])){
    $username=$_POST['username'];
    $password=$_POST['password'];

    if(DB::query('SELECT username FROM users WHERE username=:username', array(':username'=>$username))) {
        if(password_verify($password, DB::query('SELECT password FROM users WHERE username=:username', array(':username'=>$username))[0]['password'])){
            header("Location: timeline.php");
            $cstrong=True;
            $token = bin2hex(openssl_random_pseudo_bytes(64, $cstrong));
            $user_id=DB::query('SELECT id FROM users WHERE username=:username',array(':username'=>$username))[0]['id'];
            DB::query('INSERT INTO login_tokens VALUES (\'\', :token, :user_id)', array(':token'=>sha1($token), ':user_id'=>$user_id));

            setcookie("MBID", $token, time() + 60 * 60 * 24 * 7, '/', NULL, NULL, TRUE);
            setcookie("MBID_", "1", time() + 60 * 60 * 24 * 3, '/', NULL, NULL, TRUE);
        } else {
            echo 
            '<script>
            swal({
                title: "Error",
                text: "Incorrect password!",
                type: "warning"
            }, function() {
                window.location = "index.php";
            });
        </script>';
        }
    } else {
        echo 
            '<script>
            swal({
                title: "Error",
                text: "User not registered!",
                type: "warning"
            }, function() {
                window.location = "index.php";
            });
        </script>';
    }
}
?>
</body>

