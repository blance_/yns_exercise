<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<?php
class Comment {
    public static function createComment($commentBody, $postId, $userId) {
       
        if(strlen($commentBody)>140 || strlen($commentBody)<1){
            die();
        }
        if(!DB::query('SELECT id FROM posts WHERE id=:postid', array(':postid'=>$postId))) {
            die();
        } else {
            DB::query('INSERT INTO comments VALUES (\'\', :comment, :userid, NOW(), :postid)', array(':comment'=>$commentBody, ':userid'=>$userId, ':postid'=>$postId));
        }
    }

    public static function displayComment($postId){
        $comments = DB::query('SELECT comments.comment, users.username, users.profile_pic, comments.created_at FROM comments, users
        WHERE post_id = :postid
        AND comments.user_id = users.id', array(':postid'=>$postId));
        foreach ($comments as $comment) {
            echo '<li class="comment-item">
            <div class="post__author author vcard inline-items">
                <img src="'.$comment['profile_pic'].'" alt="author">
    
                <div class="author-date">
                    <a class="h6 post__author-name fn" href=profile.php?username='.$comment['username'].'>'.$comment['username'].'</a>
                    <div class="post__date">
                        <time class="published">
                            '. date('F d, Y h:i A', strtotime($comment['created_at'])).'
                        </time>
                    </div>
                </div>
    
            </div>
    
            <p>'.$comment['comment'].'</p>
        </li>';
        }
    }
}