<?php
class Pictures {
    public static function uploadProfile($image,$userid,$tmp) {
        if(!file_exists('pictures/')) {
        mkdir('pictures/', 077, true);
        }

        $dir = "pictures/";
        $picture = $dir.$image;
        $pictureTarget = $dir.basename($picture);
        // $pictureType = strtolower(pathinfo($pictureTarget, PATHINFO_EXTENSION));
        DB::query('UPDATE users SET profile_pic=:profile_pic WHERE id=:userid', array(':profile_pic'=>$picture, ':userid'=>$userid));

        if(!move_uploaded_file($tmp, $pictureTarget)) {
            echo "Failed to upload image";
        }
    }

    public static function displayProfilePic($userid){
        $pic=DB::query('SELECT profile_pic FROM users WHERE id=:userid', array(':userid'=>$userid))[0]['profile_pic'];
        return $pic;
    }

}
?>