<!DOCTYPE html>
 <html>
  <head>
   <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  </head>
  <body>
<?php 
include('classes/DB.php');

if (isset($_POST['register'])) {
    $username=$_POST['username'];
    $password=$_POST['password'];
    $email=$_POST['email'];

    if(!DB::query('SELECT username FROM users WHERE username=:username', array(':username'=>$username))) {
        if(strlen($username)>=3 && strlen($username)<=32){
            if (preg_match('/[a-zA-z0-9_]+/', $username)){
                if(strlen($password)>=6 && strlen($password)<=60){
                    if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                        if(!DB::query('SELECT email FROM users WHERE email=:email', array(':email'=>$email))){
                            DB::query('INSERT INTO users (username, password, email) VALUES (:username, :password, :email)', array(':username'=>$username, ':password'=>password_hash($password,PASSWORD_BCRYPT), ':email'=>$email));
                            echo 
                            '<script>
                            swal({
                                title: "Success",
                                text: "Registered successfully!",
                                type: "success"
                            }, function() {
                                window.location = "index.php";
                            });
                        </script>';
                        } else {
                            echo 
                        '<script>
                        swal({
                            title: "Error",
                            text: "Email already exists!",
                            type: "warning"
                        }, function() {
                            window.location = "register.php";
                        });
                    </script>';
                        }
                    } else {
                        echo 
                        '<script>
                        swal({
                            title: "Error",
                            text: "Invalid email!",
                            type: "warning"
                        }, function() {
                            window.location = "register.php";
                        });
                    </script>';
                    }
                } else {
                    echo 
                    '<script>
                    swal({
                        title: "Error",
                        text: "Invalid password!",
                        type: "warning"
                    }, function() {
                        window.location = "register.php";
                    });
                </script>';
                }
            } else {
                echo 
                    '<script>
                    swal({
                        title: "Error",
                        text: "Invalid username!",
                        type: "warning"
                    }, function() {
                        window.location = "register.php";
                    });
                </script>';
            }
        } else {
            echo 
                '<script>
                swal({
                    title: "Error",
                    text: "Username length invalid!",
                    type: "warning"
                }, function() {
                    window.location = "register.php";
                });
            </script>';
        }
    } else {
        echo 
            '<script>
            swal({
                title: "Error",
                text: "Username already exists!",
                type: "warning"
            }, function() {
                window.location = "register.php";
            });
        </script>';
    }
}
?>
</body>