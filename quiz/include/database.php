<?php 
include "db_config.php";

class Database {
    private $connection;

    function __construct() {
       $this->connect_db();
    }

    public function connect_db() {
        $this->connection = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        if(mysqli_connect_error()) {
            die("Database connection failed" . mysqli_connect_error() . mysqli_connect_errno());
        }
    }

    /*first_name, last_name, username, password, profile_pic*/
    public function register($fname, $lname, $username, $password, $profile_pic) {        
        $userQuery="SELECT * FROM accounts where username='".$username."'";
        $check=mysqli_query($this->connection,$userQuery);
        //$checkUser = mysqli_fetch_array($check);
        

        if (mysqli_num_rows($check) > 0) {
            echo "<script>
              alert('Username exists');
              window.location.href='register.php';
              </script>";
        } else {
            $target="profile_pic/".basename($profile_pic);
            $sql = "INSERT INTO accounts (first_name, last_name, username, password, profile_pic) VALUES ('$fname', '$lname', '$username', '$password', '$profile_pic')";
            $res = mysqli_query($this->connection, $sql);

            if($pic=move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
               return true;
            } else {
                return false;
            }

            if($res && $pic == TRUE) {
                return true;
            } else {
                return false;
            }
        }

       
    }

    public function check_login($username, $password){
        $password = sha1($password);
        $sql = "SELECT * FROM accounts where username = '".$username."' AND password = '".$password."'";
        $res = mysqli_query($this->connection, $sql);
        $accounts = mysqli_fetch_array($res);
        $count = $res->num_rows;

        if($count==1) {
            session_start();
            $_SESSION['login']=true;
            $_SESSION['username']=$username;
            return true;
        } else {
            return false;
        }
    }

    public function read($id=null) {
        $sql = "SELECT * FROM accounts";
        if($id) {
            $sql .= " WHERE id=$id";
        }
        $res = mysqli_query($this->connection, $sql);
        return $res;
    }

    public function update($fname, $lname, $username, $password, $profile_pic) {
        $sql = "UPDATE accounts SET first_name='$fname', last_name='$lname', username='$username', password='$password', profile_pic='$profile_pic' WHERE id=$id";
        $res = mysqli_query($this->connection, $sql);
        if($res) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id) {
        $sql = "DELETE FROM grades WHERE id=$id";
        $res = mysqli_query($this->connection, $sql);
        if($res) {
            return true;
        } else {
            return false;
        }
    }

    public function sanitize($var) {
        $return = mysqli_real_escape_string($this->connection, $var);
        return $return;
    }

    public function get_session() {
        return $_SESSION['username'];
    }

    public function user_logout() {
        $_SESSION['login']=FALSE;
        session_destroy();
    }

    public function get_fullname($username) {
        $sql = "SELECT first_name, last_name FROM accounts WHERE username='".$username."'";
        $res = mysqli_query($this->connection, $sql);
        $user=mysqli_fetch_array($res);
        echo $user['first_name'] . ' ' . $user['last_name'];
    }

    public function get_firstname($username) {
        $sql = "SELECT first_name FROM accounts WHERE username='".$username."'";
        $res = mysqli_query($this->connection, $sql);
        $user=mysqli_fetch_array($res);
        echo $user['first_name'];
    }

    public function get_lastname($username) {
        $sql = "SELECT last_name FROM accounts WHERE username='".$username."'";
        $res = mysqli_query($this->connection, $sql);
        $user=mysqli_fetch_array($res);
        echo $user['last_name'];
    }

    public function get_photo($username) {
        $sql = "SELECT profile_pic FROM accounts WHERE username='".$username."'";
        $res = mysqli_query($this->connection, $sql);
        $user=mysqli_fetch_array($res);
        echo $user['profile_pic'];
    }

     public function category($id=null) {
        $sql = "SELECT * FROM categories";
        if($id) {
            $sql .= " WHERE id=$id";
        }
        $cate = mysqli_query($this->connection, $sql);
        return $cate;
    }

    // public function arts() {
    //     $sql = "SELECT b.description answers FROM questions a inner join answers b where a.id=1 && b.questions_id=a.id";
    //     $aq = mysqli_query($this->connection, $sql);
    //     return $aq;
    // }

    // public function art_question() {
    //     $sql = "SELECT a.questions questions1 FROM questions a inner join categories c where a.category_id=1";
    //     $ques = mysqli_query($this->connection, $sql);
    //     return $ques;
    // }

    // public function history($username) {
    //     $sql="SELECT * FROM grades a inner join categories b where b.id=a.category_id && username='".$username."'";
    //     $grades=mysqli_query($this->connection, $sql);
    //     return $grades;
    // }
}

$database = new Database();