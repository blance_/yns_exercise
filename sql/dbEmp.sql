
CREATE TABLE `employees` (
    `emp_id` int(11) NOT NULL AUTO_INCREMENT,
    `first_name` varchar(50),
    `last_name` varchar(50),
    `middle_name` varchar(50),
    `department_id` int(11) NOT NULL,
    `hire_date` date,
    `boss_id` varchar(11),
    PRIMARY KEY (`emp_id`)
);

CREATE TABLE `departments` (
    `department_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50),
    PRIMARY KEY (`department_id`)
);

CREATE TABLE `positions` (
    `position_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50),
    PRIMARY KEY (`position_id`)
);

CREATE TABLE `employee_positions` (
    `emppos_id` int(11) NOT NULL AUTO_INCREMENT,
    `emp_id` int(11) NOT NULL,
    `position_id` int(11) NOT NULL,
    PRIMARY KEY (`emppos_id`),
    FOREIGN KEY (`position_id`) REFERENCES `positions`(`position_id`)
);

INSERT INTO `employees` (`first_name`, `last_name`, `department_id`) VALUES ('Manabu', 'Yamazaki', 1);
INSERT INTO `employees` (`first_name`, `last_name`, `department_id`, `hire_date`, `boss_id`) VALUES ('Tomohiko','Takasago',3,'2014-4-1',1);
INSERT INTO `employees` (`first_name`, `last_name`, `department_id`, `hire_date`, `boss_id`) VALUES ('Yuta', 'Kawakami',4,'2014-4-1',1);
INSERT INTO `employees` (`first_name`, `last_name`, `department_id`, `hire_date`, `boss_id`) VALUES ('Shogo', 'Kubota',4,'2014-12-1',1);
INSERT INTO `employees` (`first_name`, `last_name`, `middle_name`, `department_id`, `hire_date`, `boss_id`) VALUES ('Lorraine', 'San Jose', 'P.', 2,'2015-3-10',1);
INSERT INTO `employees` (`first_name`, `last_name`, `middle_name`, `department_id`, `hire_date`, `boss_id`) VALUES ('Haille', 'Dela Cruz', 'A.', 3,'2015-2-15',2);
INSERT INTO `employees` (`first_name`, `last_name`, `middle_name`, `department_id`, `hire_date`, `boss_id`) VALUES ('Godfrey', 'Sarmenta', 'L.', 4,'2015-1-1',1);
INSERT INTO `employees` (`first_name`, `last_name`, `middle_name`, `department_id`, `hire_date`, `boss_id`) VALUES ('Alex', 'Amistad', 'F.', 4,'2015-4-10',1);
INSERT INTO `employees` (`first_name`, `last_name`, `department_id`, `hire_date`, `boss_id`) VALUES ('Hideshi', 'Ogoshi', 4,'2014-6-1',1);
INSERT INTO `employees` (`first_name`, `department_id`, `hire_date`, `boss_id`) VALUES ('Kim', 5,'2015-8-6',1);

INSERT INTO `departments` (`name`) VALUES ('Executive');
INSERT INTO `departments` (`name`) VALUES ('Admin');
INSERT INTO `departments` (`name`) VALUES ('Sales');
INSERT INTO `departments` (`name`) VALUES ('Development');
INSERT INTO `departments` (`name`) VALUES ('Design');
INSERT INTO `departments` (`name`) VALUES ('Marketing');

INSERT INTO `positions` (`name`) VALUES ('CEO');
INSERT INTO `positions` (`name`) VALUES ('CTO');
INSERT INTO `positions` (`name`) VALUES ('CFO');
INSERT INTO `positions` (`name`) VALUES ('Manager');
INSERT INTO `positions` (`name`) VALUES ('Staff');

INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (1,1);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (1,2);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (1,3);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (2,4);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (3,5);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (4,5);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (5,5);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (6,5);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (7,5);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (8,5);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (9,5);
INSERT INTO `employee_positions` (`emp_id`, `position_id`) VALUES (10,5);



