<!DOCTYPE html>
<html>
<head>
    <title>Exer 1-9</title>

    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <table style="width:100%;">
    <tr>
        <th>ID</th>
        <th>User Name</th>
        <th>First Name</th>
        <th>Middle Initial</th>
        <th>Last Name</th>
        <th>Email Address</th>
        <th>Address</th>
        <th>Zip Code</th>
        <th>Image</th>
    </tr>
        <?php
            $f = fopen("data.csv", "r");
            $i=1;
            while (($line = fgetcsv($f,1000,",")) !== false) {
                echo "<tr>";
                echo "<td>" . $i . "</td>";
                echo "<td>" . $line[0] . "</td>";
                echo "<td>" . $line[2] . "</td>";
                echo "<td>" . $line[3] . "</td>";
                echo "<td>" . $line[4] . "</td>";
                echo "<td>" . $line[5] . "</td>";
                echo "<td>" . $line[6] . "</td>";
                echo "<td>" . $line[7] . "</td>";
                echo "<td><img src='" . $line[8] . "' height=100 width=100></td>";
                echo "</tr>";
            $i++;  
            }
            fclose($f);
            echo "\n";
        ?>
    </table>
</body>
</html>
