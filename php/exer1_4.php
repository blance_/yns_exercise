<!DOCTYPE html>
<html>
<head>
    <title>FizzBuzz Problem</title>
</head>
<body>
    <h2> FizzBuzz Problem</h2>
    <form action="" method="post">
        <p> Enter a number: </p>
        <input type="text" name="num">
        <input type="submit" name="submit" value="Submit">
    </form>

    <?php
    if (isset($_POST['submit'])){
        if (!empty($_POST['num'])) {
            if (is_numeric($_POST['num'])){ 
                $calc=mysql_real_escape_string($_POST['num']);
                for ($i=1;$i<=$calc;$i++){
                    if (($i%3==0) && ($i%5==0)){
                        echo "FizzBuzz";
                        echo '<br>';
                    }
                    else if($i%3==0)  {
                        echo "Fizz";
                        echo '<br>';
                    }
                    else if ($i%5==0) {
                        echo "Buzz";
                        echo '<br>';
                    }
                    else {
                        echo $i;
                        echo '<br>';
                    }
                }
            } else {
                echo "Input a valid number";
            }
        } else {
            echo "Complete the field!";
        }
    }

    ?>

</body>
</html>