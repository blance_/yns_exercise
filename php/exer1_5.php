<!DOCTYPE html>
<html>
<head>
    <title>Date</title>
</head>
<body>
    <form action="" method="post">
        <p> Enter a date: </p>
        <input type="date" name="txtdate">
        <input type="submit" name="submit" value="Submit">
    </form>

    <?php
        if(isset($_POST['submit'])) {
            $date=$_POST['txtdate'];
            echo date('F d, Y D', strtotime($date.'+3days'));
        }
    ?>
</body>
</html>