<?php
include('classes/DB.php');
include('classes/Login.php');
include('classes/Pictures.php');
// echo json_encode($_FILES);
$userid=Login::isLoggedIn();
if(isset($_POST['uploadprofileimg'])) {
    
    $picture=$_FILES['profileimg']['name'];
    $temp=$_FILES['profileimg']['tmp_name'];
    Pictures::uploadProfile($picture,$userid,$temp);
}
?>
<h2>Account</h2>
<form action="myaccount.php" method="post" enctype="multipart/form-data">
    Upload picture:
    <input type="file" name="profileimg">
    <input type="submit" name="uploadprofileimg" value="Upload Image">
</form>