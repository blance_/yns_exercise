<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<?php
	include('classes/DB.php');
	include('classes/Login.php');
	include('classes/Post.php');
	include('classes/Pictures.php');
	include('classes/Comment.php');
	

	if(Login::isLoggedIn()){
		$userid=Login::isLoggedIn();
		$showTimeline=true;
	} else {
		echo '<script>
            swal({
                title: "Error",
                text: "Not logged in",
                type: "warning"
            }, function() {
                window.location = "index.php";
            });
        </script>';
	}

	if(isset($_GET['postid'])){
		Post::likesPost($_GET['postid'], $userid);
	}
	if(isset($_POST['comment'])){
		Comment::createComment($_POST['commentbody'], $_GET['postid'], $userid);
	}
	
	$username = DB::query('SELECT username FROM users WHERE id=:userid', array('userid'=>$userid))[0]['username'];

	$showTimeline=false;
	
	if(isset($_GET['searchbox'])) {
		$users = DB::query('SELECT username FROM users WHERE username LIKE :username', array(':username'=>'%'.$_GET['searchbox'].'%'));
		print_r($users);
		die();
	}

	?>

	<title>Timeline - MicroBlog</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Main Font -->
	<script src="js/webfontloader.min.js"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="Bootstrap/dist/css/bootstrap-reboot.css">
	<link rel="stylesheet" type="text/css" href="Bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="Bootstrap/dist/css/bootstrap-grid.css">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="css/main.min.css">
	<link rel="stylesheet" type="text/css" href="css/fonts.min.css">


</head>
<body>

<!-- Fixed Sidebar Left -->

<div class="fixed-sidebar">
	<div class="fixed-sidebar-left sidebar--small" id="sidebar-left">

		<a href="timeline.php" class="logo">
			<div class="img-wrap">
				<img src="img/logo.png" alt="MicroBlog">
			</div>
		</a>

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<ul class="left-menu">
				<li>
					<a href="#" class="js-sidebar-open">
						<svg class="olymp-menu-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="OPEN MENU"><use xlink:href="svg-icons/sprites/icons.svg#olymp-menu-icon"></use></svg>
					</a>
				</li>
				<li>
					<a href="timeline.php">
						<svg class="olymp-newsfeed-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="NEWSFEED"><use xlink:href="svg-icons/sprites/icons.svg#olymp-newsfeed-icon"></use></svg>
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1">
		<a href="newsfeed.php" class="logo">
			<div class="img-wrap">
				<img src="img/logo.png" alt="Olympus">
			</div>
			<div class="title-block">
				<h6 class="logo-title">microblog</h6>
			</div>
		</a>

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<ul class="left-menu">
				<li>
					<a href="#" class="js-sidebar-open">
						<svg class="olymp-close-icon left-menu-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
						<span class="left-menu-title">Collapse Menu</span>
					</a>
				</li>
				<li>
					<a href="03-Newsfeed.html">
						<svg class="olymp-newsfeed-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="NEWSFEED"><use xlink:href="svg-icons/sprites/icons.svg#olymp-newsfeed-icon"></use></svg>
						<span class="left-menu-title">Timeline</span>
					</a>
				</li>
		</div>
	</div>
</div>

<!-- ... end Fixed Sidebar Left -->

<header class="header" id="site-header">

	<div class="page-title">
		<h6>Timeline</h6>
	</div>

	<div class="header-content-wrapper">
		<form class="search-bar w-search notification-list friend-requests">
			<div class="form-group with-button">
				<input class="form-control" placeholder="Search here people or pages..." type="text" name="searchbox">
			</div>
		</form>

		<div class="control-block">

			<div class="author-page author vcard inline-items more">
				<div class="author-thumb">
					<img alt="author" src="<?php echo Pictures::displayProfilePic($userid)?>" height="50" width="50" class="img-thumbnail">
					<div class="more-dropdown more-with-triangle">
						<div class="mCustomScrollbar" data-mcs-theme="dark">
							<div class="ui-block-title ui-block-title-small">
								<h6 class="title">Your Account</h6>
							</div>

							<ul class="account-settings">
								<li>
									<a href="settings.php">

										<svg class="olymp-menu-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-menu-icon"></use></svg>

										<span>Profile Settings</span>
									</a>
								</li>
								<li>
									<a href="logout.php">
										<svg class="olymp-logout-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-logout-icon"></use></svg>

										<span>Log Out</span>
									</a>
								</li>
							</ul>
						</div>

					</div>
				</div>
				<a href="settings.php" class="author-name fn">
					<div class="author-title">
					<?php echo $username ?> <svg class="olymp-dropdown-arrow-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
					</div>
				</a>
			</div>

		</div>
	</div>

</header>
<!-- ... end Responsive Header-BP -->
<div class="header-spacer"></div>


<div class="container">
	<div class="row">
		<div class="col col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="clients-grid">
				<div class="row sorting-container" id="clients-grid-1" data-layout="masonry">

					<div class="col-md-12  sorting-item family animals natural politics">

						

								<?php
								
								$followingpost=DB::query('SELECT posts.created_at, posts.id, posts.body, posts.likes, users.username, users.profile_pic FROM users, posts, followers WHERE posts.user_id=followers.user_id 
								AND users.id=posts.user_id
								AND follower_id=:userid
								ORDER BY posts.created_at ASC', array(':userid'=>$userid));

								foreach($followingpost as $post){
										echo "
										<div class='ui-block'>
										<article class='hentry post'>
										<div class='post__author author vcard inline-items'>
											<img src='".$post['profile_pic'] ."' alt='Author'>
											<div class='author-date'>";

												echo "<a class='h6 post__author-name fn' href='profile.php?username=".$post['username']."'>".$post['username']."</a>
												<div class='post__date'>
													<time class='published'>
														";
														echo date('F d, Y h:i A', strtotime($post['created_at']));
													echo "</time>
												</div>
											</div>

										<div class='more'><svg class='olymp-three-dots-icon'><use xlink:href='svg-icons/sprites/icons.svg#olymp-three-dots-icon'></use></svg>
											<ul class='more-dropdown'>
												<li>
													<a href='#'>Edit Post</a>
												</li>
												<li>
													<a href='#'>Delete Post</a>
												</li>
												<li>
													<a href='#'>Turn Off Notifications</a>
												</li>
												<li>
													<a href='#'>Select as Featured</a>
												</li>
											</ul>
										</div>
										</div>";
										echo '<p>'.$post['body'].'</p>';
										echo "<div class='post-additional-info inline-items'>

										<form action='timeline.php?postid=".$post['id']."' method='post'>";
										if(!DB::query('SELECT post_id FROM post_likes WHERE post_id=:postid AND user_id=:userid', array(':postid'=>$post['id'], ':userid'=>$userid))){
											echo "<input type='submit' name='like' value='Like'>";
										} else {
											echo "<input type='submit' name='unlike' value='Unlike'>";
										}

										echo "</form>
							
										<a href='#' class='post-add-icon inline-items'>
											<svg class='olymp-heart-icon'><use xlink:href='svg-icons/sprites/icons.svg#olymp-heart-icon'></use></svg>
											<span>".$post['likes'].'&nbsp likes'."</span>
										</a>
								
										
								
								
										<div class='comments-shared'>
											<a href='' class='post-add-icon inline-items'>
												<svg class='olymp-speech-balloon-icon'><use xlink:href='svg-icons/sprites/icons.svg#olymp-speech-balloon-icon'></use></svg>
												<span>0</span>
											</a>
								
											<a href='' class='post-add-icon inline-items'>
												<svg class='olymp-share-icon'><use xlink:href='svg-icons/sprites/icons.svg#olymp-share-icon'></use></svg>
												<span>0</span>
											</a>
										</div>
								
								
									</div>
								
									<div class='control-block-button post-control-button'>
																		
										<a href='' class='btn btn-control'>
											<svg class='olymp-share-icon'><use xlink:href='svg-icons/sprites/icons.svg#olymp-share-icon'></use></svg>
										</a>
									</div>

									</article>";
									echo "<ul class='comments-list'>
									<li class='comment-item'>";
									echo Comment::displayComment($post['id']);
									echo '</li>
									</ul>';
									echo "<form class='comment-form inline-items' action='timeline.php?postid=".$post['id']."' method='post'>
							
									<div class='post__author author vcard inline-items'>
										<img src='".Pictures::displayProfilePic($userid)."' alt='author'>
								
										<div class='form-group with-icon-right is-empty'>
											<textarea class='form-control' placeholder='Write a comment' name='commentbody'></textarea>
										</div>
										<button type='submit' name='comment' class='btn btn-md-2 btn-primary'>Post Comment</button>
									</div>
									<br>
									
								</form>
								
								</div>";
										
										// echo "<form action='index.php?postid=".$post['id']."' method='post'>";
										// if(!DB::query('SELECT post_id FROM post_likes WHERE post_id=:postid AND user_id=:userid', array(':postid'=>$post['id'], ':userid'=>$userid))){
										// 	echo "<input type='submit' name='like' value='Like'>";
										// } else {
										// 	echo "<input type='submit' name='like' value='Unlike'>";
										// }
										// echo "<span>".$post['likes']." likes</span>
										// </form>
								}
								?>
						
					</div>
					<!--end post-->
				</div>
			</div>
		</div>
	</div>
</div>


<!-- JS Scripts -->
<script src="js/jquery-3.2.1.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/perfect-scrollbar.js"></script>
<script src="js/jquery.matchHeight.js"></script>
<script src="js/svgxuse.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/Headroom.js"></script>
<script src="js/velocity.js"></script>
<script src="js/ScrollMagic.js"></script>
<script src="js/jquery.waypoints.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/smooth-scroll.js"></script>
<script src="js/selectize.js"></script>
<script src="js/swiper.jquery.js"></script>
<script src="js/moment.js"></script>
<script src="js/isotope.pkgd.js"></script>
<script src="js/ajax-pagination.js"></script>
<script src="js/Chart.js"></script>
<script src="js/chartjs-plugin-deferred.js"></script>
<script src="js/circle-progress.js"></script>
<script src="js/loader.js"></script>
<script src="js/run-chart.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/jquery.gifplayer.js"></script>

<script src="js/base-init.js"></script>
<script defer src="fonts/fontawesome-all.js"></script>

<script src="Bootstrap/dist/js/bootstrap.bundle.js"></script>

</body>
</html>