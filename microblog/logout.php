<?php
include('classes/DB.php');
include('classes/Login.php');

if(!Login::isLoggedIn()){
    die("Not logged in");
}

if(isset($_POST['confirm'])){
    if(isset($_POST['alldevices'])){
        DB::query('DELETE FROM login_tokens WHERE user_id=:user_id', array(':user_id'=>Login::isLoggedIn()));
    } else {
        if (isset($_COOKIE['MBID'])){
            DB::query('DELETE FROM login_tokens WHERE token=:token', array(':token'=>sha1($_COOKIE['MBID'])));
        }
        if(isset($_COOKIE['MBID'])) {
            unset($_COOKIE['MBID']);
            unset($_COOKIE['MBID_']);
            setcookie('MBID', null, -1, '/');
            setcookie('MBID_', null, -1, '/');
        }
    }
}
?>
<h2>Logout</h2>
<form action="logout.php" method="post">
    <input type="checkbox" name="alldevices" value="">Logout of all devices?</br>   
    <input type="submit" name="confirm" value="Confirm">
</form>