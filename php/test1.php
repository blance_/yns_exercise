<!DOCTYPE html>
<html>
<head>
    <title>Exer 1-9</title>

    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <table style="width:100%;">
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Middle Initial</th>
        <th>Last Name</th>
        <th>Email Address</th>
        <th>Address</th>
        <th>Zip Code</th>
        <th>Image</th>

    </tr>

        <?php
            $f = fopen("data.csv", "r");
            
            $i=1;
            while (($line = fgetcsv($f,1000,",")) !== false){
               
                echo "<tr>";
               
                
                echo "<td>" . $i . "</td>";
                    
                    foreach ($line as $cell) {

                        if (file_exists($cell)) {
                            echo "<td><img src=" . htmlspecialchars($cell) . " height=100 width=100></td>";
                        } else {
                            echo "<td>" . htmlspecialchars($cell) . "</td>";
                        }
                    }
                
                echo "</tr>";
            $i++;  
            }
        
            fclose($f);
            echo "\n";
        ?>
    </table>

    <div class="pagination">
        <a href="#">&laquo;</a>
        <a href="#">1</a>
        <a href="#">2</a>
        <a href="#">3</a>
        <a href="#">4</a>
        <a href="#">5</a>
        <a href="#">6</a>
        <a href="#">&raquo;</a>
    </div>
</body>
</html>
