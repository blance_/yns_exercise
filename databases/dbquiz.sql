-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2018 at 03:07 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbquiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `profile_pic` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `first_name`, `last_name`, `username`, `password`, `profile_pic`) VALUES
(1, 'Blances', 'Sanchez', 'blance', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'profile_pic/jbpr-recruit-header.jpeg'),
(12, 'Test1', 'Test1', 'blance2', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'profile_pic/3.png');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `correct` varchar(255) NOT NULL,
  `option1` varchar(255) NOT NULL,
  `option2` varchar(255) NOT NULL,
  `option3` varchar(255) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `correct`, `option1`, `option2`, `option3`, `question_id`) VALUES
(1, 'Red, yellow and blue.', 'Red, yellow and green.', 'Red, yellow and blue.', 'Red, violet and blue.', 1),
(2, 'An unfinished object.', 'An unidentified object.', 'An unfinished option.', 'An unfinished object.', 2),
(3, 'Rudolf Nureyev', 'Rudolf Nureyev', 'Rudolf Pasteur', 'Rudolf Marcus', 3),
(4, 'The Mona Lisa', 'The Mona ', 'The Lisa', 'The Mona Lisa', 4),
(5, 'To be played softly', 'To be played softly', 'To be played loudly', 'To be played harmoniously', 5),
(6, 'Pablo Picasso', 'Pablo Picasso', 'Leonardo Da Vinci', 'Michaelangelo', 6),
(7, 'Three', 'Three', 'Four', 'Five', 7),
(8, 'Dante Gabriel Rossetti', 'Michael Gabriel Rossetti', 'Leo Gabriel Rossetti', 'Dante Gabriel Rossetti', 8),
(9, 'Egg yolk', 'Egg white', 'Egg shell', 'Egg yolk', 9),
(10, 'Pottery', 'Pottery', 'Lottery', 'Clay', 10);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `link`) VALUES
(1, 'Arts', 'From symbolism to sculpture, this quiz will put you in touch with your artistic side.', 'category/arts.php');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `test_taken` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_id` int(11) NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `username`, `test_taken`, `category_id`, `score`) VALUES
(1, 'blance', '2018-06-25 02:25:37', 1, 5),
(2, 'blance', '2018-06-25 02:25:43', 1, 5),
(3, 'blance', '2018-06-25 02:27:29', 1, 5),
(4, 'blance', '2018-06-25 02:35:02', 1, 5),
(5, 'blance', '2018-06-25 02:35:38', 1, 5),
(6, 'blance', '2018-06-25 02:35:55', 1, 5),
(7, 'blance', '2018-06-25 02:36:12', 1, 5),
(8, 'blance', '2018-06-25 03:07:44', 1, 10),
(9, 'blance', '2018-06-25 03:07:50', 1, 10),
(10, 'blance', '2018-06-25 03:08:19', 1, 10),
(11, 'blance', '2018-06-25 03:08:28', 1, 10),
(12, 'blance', '2018-06-25 03:10:12', 1, 2),
(13, 'blance', '2018-06-25 03:43:17', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `questions_name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `questions_name`, `category_id`) VALUES
(1, 'Name the three primary colours.', 1),
(2, 'In needlework, what does UFO refer to?', 1),
(3, 'Name the famous ballet Russian dancer who changed the face of modern ballet. ', 1),
(4, 'What is the painting \'La Gioconda\' more usually known as?', 1),
(5, 'What does the term \'piano\' mean?', 1),
(6, 'Name the Spanish artist, sculptor and draughtsman famous for co-founding the Cubist movement.', 1),
(7, 'How many valves does a trumpet have?', 1),
(8, 'Who painted How Sir Galahad, Sir Bors, and Sir Percival were Fed with the Sanc Grael; But Sir Percival\'s Sister Died by the Way?', 1),
(9, 'If you were painting with tempera, what would you be using to bind together colour pigments?', 1),
(10, 'What is John Leach famous for making?', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
