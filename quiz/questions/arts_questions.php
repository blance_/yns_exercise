<?php
session_start();
include('../include/database.php');

$database=new Database();

$username=$_SESSION['username'];

if(!$database->get_session()){
  header("location:index.php");
}

if(isset($_GET['o'])){
  $database->user_logout();
  header("location:../index.php");
} 
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quiz</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../dist/css/style.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href=".#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">Q</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Quiz</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../<?php $database->get_photo($username);?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $database->get_fullname($username);?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../<?php $database->get_photo($username);?>" class="img-circle" alt="User Image">

                <p>
                  <?php $database->get_fullname($username);?>
                  <!-- <small>Member since Nov. 2012</small> -->
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="../profile.php" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="arts_questions.php?o=logout" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../<?php $database->get_photo($username);?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $database->get_fullname($username);?></p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="../home.php"><i class="fa fa-pencil"></i> <span>Quiz</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h2>
        Category: Arts
      </h2>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Employees</li>
      </ol> -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="box-body">
          <!-- <h3 id="quiz-time-left"></h3>
          <span style="color:red">Time left</span> -->
          <br><br><br>
          <form name="quiz" action="result.php">
            <?php 
            $con=mysqli_connect('localhost','root','','dbquiz');
            $sql="select b.question_id id, a.body questions, b.option1 x, b.option2 y, b.option3 z from questions a inner join answers b where a.id=b.question_id";
            $query= mysqli_query($con,$sql);
            $numrows=mysqli_num_rows($query);
            ?>
            <form method="post" action="result.php">
            <?php
            $i=1;
            if($numrows !=0 ){
                while($row=mysqli_fetch_assoc($query)) {
                  ?>
                  <p><?php echo $i .'. '. $row['questions'];?></p>
                  <input type="radio" name="<?php echo 'Ans' . $row['id']?>" value="<?php echo $row['x'];?>"><span><?php echo $row['x'];?></span><br>
                  <input type="radio" name="<?php echo 'Ans' . $row['id']?>" value="<?php echo $row['y'];?>"><span><?php echo $row['y'];?></span><br>
                  <input type="radio" name="<?php echo 'Ans' . $row['id']?>" value="<?php echo $row['z'];?>"><span><?php echo $row['z'];?></span><br><br>

                  <?php
                  $i++;
                }
              }
              ?>
           <!--<input type="submit" name="submit" value="submit">-->
           <input type="submit" onclick="formSubmit();" class="btn btn-primary" value="Submit">
           <!-- <input type="hidden" name="timeleft" id="timeleft"> -->
           
          
          </form>
        </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      
    </div>
    <strong>Copyright &copy; 2018 <a href="#">Blances Sanchez</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- page script -->
<!-- <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script> -->

<!-- <script type="text/javascript">
  var total_seconds=2;
  var c_minutes = parseInt(total_seconds/60);
  var c_seconds = parseInt(total_seconds%60);
  //var timeleft=document.getElementById("timeleft").value = total_seconds;
  function CheckTime() {
    document.getElementById("quiz-time-left").innerHTML= c_minutes + ' minutes ' + c_seconds + ' seconds';
    if(total_seconds <=0) {
      setTimeout('document.quiz.submit()',1);
    } else {
      total_seconds=total_seconds-1;
      c_minutes=parseInt(total_seconds/60);
      c_seconds=parseInt(total_seconds%60);
      setTimeout("CheckTime()",1000);
    }
  }
  setTimeout("CheckTime()",1000);

  function formSubmit(){
    document.quiz.submit();
  }
</script> -->
</body>
</html>
