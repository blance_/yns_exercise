<?php 

include('database.php');
?>
<!DOCTYPE html>
<html>
<head>
    <title>SQL Exercises</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <p> 1) Retrieve employees whose last name start with "K". </p>
    <table>
        <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Middle Name</th>
            <th>Department ID</th>
            <th>Hire Date</th>
            <th>Boss ID</th>
        </tr>
    <?php
        $sql="SELECT * FROM employees WHERE last_name LIKE 'k%'";
        if($result=mysqli_query($conn,$sql)) {
            if(mysqli_num_rows($result)>0){
                while ($row=mysqli_fetch_array($result)) {
                    echo "<tr>";
                        echo "<td>" . $row['emp_id'] . "</td>";
                        echo "<td>" . $row['first_name'] . "</td>";
                        echo "<td>" . $row['last_name'] . "</td>";
                        echo "<td>" . $row['middle_name'] . "</td>";
                        echo "<td>" . $row['department_id'] . "</td>";
                        echo "<td>" . $row['hire_date'] . "</td>";
                        echo "<td>" . $row['boss_id'] . "</td>";
                    echo "</tr>";

                }
                mysqli_free_result($result);

            } else {
                echo "No records matching your query were found.";
            }
        } else {
            echo "ERROR: Could not able to execute SQL" . mysqli_error($conn);
        }
    ?>
    </table>

     <p> 2) Retrieve employees whose last name end with "i". </p>
    <table>
        <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Middle Name</th>
            <th>Department ID</th>
            <th>Hire Date</th>
            <th>Boss ID</th>
        </tr>
    <?php
        $sql="SELECT * FROM employees WHERE last_name LIKE '%i'";
        if($result=mysqli_query($conn,$sql)) {
            if(mysqli_num_rows($result)>0){
                while ($row=mysqli_fetch_array($result)) {
                    echo "<tr>";
                        echo "<td>" . $row['emp_id'] . "</td>";
                        echo "<td>" . $row['first_name'] . "</td>";
                        echo "<td>" . $row['last_name'] . "</td>";
                        echo "<td>" . $row['middle_name'] . "</td>";
                        echo "<td>" . $row['department_id'] . "</td>";
                        echo "<td>" . $row['hire_date'] . "</td>";
                        echo "<td>" . $row['boss_id'] . "</td>";
                    echo "</tr>";

                }
                mysqli_free_result($result);

            } else {
                echo "No records matching your query were found.";
            }
        } else {
            echo "ERROR: Could not able to execute SQL" . mysqli_error($conn);
        }
    ?>
    </table>

    <p> 3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date. </p>
    <table>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Middle Name</th>
            <th>Hire Date</th>
        </tr>
    <?php
        $sql="SELECT first_name, last_name, middle_name,hire_date FROM employees WHERE hire_date BETWEEN '2015-1-1' AND '2015-3-31' ORDER BY hire_date ASC";
        if($result=mysqli_query($conn,$sql)) {
            if(mysqli_num_rows($result)>0){
                while ($row=mysqli_fetch_array($result)) {
                    echo "<tr>";
                        echo "<td>" . $row['first_name'] . "</td>";
                        echo "<td>" . $row['last_name'] . "</td>";
                        echo "<td>" . $row['middle_name'] . "</td>";
                        echo "<td>" . $row['hire_date'] . "</td>";
                    echo "</tr>";

                }
                mysqli_free_result($result);

            } else {
                echo "No records matching your query were found.";
            }
        } else {
            echo "ERROR: Could not able to execute SQL" . mysqli_error($conn);
        }
    ?>
    </table>

    <p> 4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show. </p>
    <table>
        <tr>
            <th>Last Name</th>
            <th>Boss Last Name</th>
        </tr>
    <?php
        $sql="SELECT a.last_name lastname, b.last_name boss FROM employees a JOIN employees b ON a.boss_id=b.emp_id WHERE a.boss_id IS NOT NULL";
        if($result=mysqli_query($conn,$sql)) {
            if(mysqli_num_rows($result)>0){
                while ($row=mysqli_fetch_array($result)) {
                    echo "<tr>";
                        echo "<td>" . $row['lastname'] . "</td>";
                        echo "<td>" . $row['boss'] . "</td>";
                    echo "</tr>";

                }
                mysqli_free_result($result);

            } else {
                echo "No records matching your query were found.";
            }
        } else {
            echo "ERROR: Could not able to execute SQL" . mysqli_error($conn);
        }
    ?>
    </table>

    <p> 5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.</p>
    <table>
        <tr>
            <th>Last Name</th>
        </tr>
    <?php
        $sql=" SELECT last_name FROM employees WHERE department_id=3 ORDER BY last_name DESC";
        if($result=mysqli_query($conn,$sql)) {
            if(mysqli_num_rows($result)>0){
                while ($row=mysqli_fetch_array($result)) {
                    echo "<tr>";
                        echo "<td>" . $row['last_name'] . "</td>";
                    echo "</tr>";

                }
                mysqli_free_result($result);

            } else {
                echo "No records matching your query were found.";
            }
        } else {
            echo "ERROR: Could not able to execute SQL" . mysqli_error($conn);
        }
    ?>
    </table>

     <p> 6) Retrieve number of employee who has middle name.</p>
    <table>
        <tr>
            <th>Number of Employee with Middle Name</th>
        </tr>
    <?php
        $sql="SELECT COUNT(middle_name) AS numOfMid FROM employees WHERE middle_name IS NOT NULL";
        if($result=mysqli_query($conn,$sql)) {
            if(mysqli_num_rows($result)>0){
                while ($row=mysqli_fetch_array($result)) {
                    echo "<tr>";

                        echo "<td>" . $row['numOfMid'] . "</td>";
                    echo "</tr>";

                }
                mysqli_free_result($result);

            } else {
                echo "No records matching your query were found.";
            }
        } else {
            echo "ERROR: Could not able to execute SQL" . mysqli_error($conn);
        }
    ?>
    </table>

     <p> 7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.</p>
    <table>
        <tr>
            <th>Department Name</th>
            <th>Number of Employee</th>
        </tr>
    <?php
        $sql="SELECT b.name department, count(*) AS employee_no FROM employees a INNER JOIN departments b ON a.department_id=b.department_id GROUP BY b.department_id";
        if($result=mysqli_query($conn,$sql)) {
            if(mysqli_num_rows($result)>0){
                while ($row=mysqli_fetch_array($result)) {
                    echo "<tr>";

                        echo "<td>" . $row['department'] . "</td>";
                         echo "<td>" . $row['employee_no'] . "</td>";
                    echo "</tr>";

                }
                mysqli_free_result($result);

            } else {
                echo "No records matching your query were found.";
            }
        } else {
            echo "ERROR: Could not able to execute SQL" . mysqli_error($conn);
        }
    ?>
    </table>

    <p> 8) Retrieve employee's full name and hire date who was hired the most recently.</p>
    <table>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Middle Name</th>
            <th>Hire Date</th>
        </tr>
    <?php
        $sql="SELECT first_name, last_name,middle_name,hire_date FROM employees ORDER BY hire_date DESC LIMIT 1";
        if($result=mysqli_query($conn,$sql)) {
            if(mysqli_num_rows($result)>0){
                while ($row=mysqli_fetch_array($result)) {
                    echo "<tr>";
                        echo "<td>" . $row['first_name'] . "</td>";
                        echo "<td>" . $row['last_name'] . "</td>";
                        echo "<td>" . $row['middle_name'] . "</td>";
                        echo "<td>" . $row['hire_date'] . "</td>";
                    echo "</tr>";

                }
                mysqli_free_result($result);

            } else {
                echo "No records matching your query were found.";
            }
        } else {
            echo "ERROR: Could not able to execute SQL" . mysqli_error($conn);
        }
    ?>
    </table>

    <p> 9) Retrieve department name which has no employee.</p>
    <table>
        <tr>
            <th>Department Name</th>
        </tr>
    <?php
        $sql="SELECT a.name department FROM departments a WHERE not exists(SELECT * FROM employees b WHERE a.department_id=b.department_id)";
        if($result=mysqli_query($conn,$sql)) {
            if(mysqli_num_rows($result)>0){
                while ($row=mysqli_fetch_array($result)) {
                    echo "<tr>";
                        echo "<td>" . $row['department'] . "</td>";
                    echo "</tr>";

                }
                mysqli_free_result($result);

            } else {
                echo "No records matching your query were found.";
            }
        } else {
            echo "ERROR: Could not able to execute SQL" . mysqli_error($conn);
        }
    ?>
    </table>

    <p> 10) Retrieve employee's full name who has more than 2 positions.</p>
    <table>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Middle Name</th>
            
        </tr>
    <?php
        $sql="SELECT a.last_name lastname, a.first_name firstname, a.middle_name middlename from employees a INNER JOIN employee_positions b where a.emp_id=b.emp_id GROUP BY a.emp_id HAVING COUNT(*)>1";
        if($result=mysqli_query($conn,$sql)) {
            if(mysqli_num_rows($result)>0){
                while ($row=mysqli_fetch_array($result)) {
                    echo "<tr>";
                        echo "<td>" . $row['firstname'] . "</td>";
                        echo "<td>" . $row['lastname'] . "</td>";
                        echo "<td>" . $row['middlename'] . "</td>";
                        
                    echo "</tr>";

                }
                mysqli_free_result($result);

            } else {
                echo "No records matching your query were found.";
            }
        } else {
            echo "ERROR: Could not able to execute SQL" . mysqli_error($conn);
        }
    ?>
    </table>



</body>
</html>
<?php


?>