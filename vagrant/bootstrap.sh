#!/usr/bin/env bash

#DOCUMENT_ROOT='/vagrant' # change if using a subdirectory in your project
MYSQL_ROOT_PASSWORD=''
#DROP_DB_IF_EXISTS=0 # Set to 1 to drop databases that exist
#DATABASES=(cra) # Space delimited
#DRUPAL_DATABASE=cra
#DRUPAL_THEME=cra
#SMTP_RELAY='1.2.3.4'

# newer repo
#rpm -Uvh https://mirror.webtatic.com/yum/el6/latest.rpm

#yum install -y httpd php54w php54w-gd php54w-mysqlnd php54w-xml

# turn off selinux
#sed -i "s/^SELINUX=.*/SELINUX=permissive/g" /etc/selinux/config
#echo "0" >/selinux/enforce

#mkdir -p /root/.provisioning

#cat <<CONFIG > /etc/httpd/conf.d/vhosts.conf
#NameVirtualHost *:80
#<VirtualHost *:80>
#        ServerAdmin webmaster@localhost
#        DocumentRoot /vagrant
#        php_flag display_startup_errors on
#        php_flag display_errors on
#        <Directory />
#          Options FollowSymLinks
#          AllowOverride All
#          Order allow,deny
#          allow from all
#        </Directory>
#        <Directory /vagrant>
#                Options Indexes FollowSymLinks MultiViews
#                AllowOverride All
#                Order allow,deny
#                allow from all
#        </Directory>
#</VirtualHost>
#CONFIG

#aervice httpd restart

### MYSQL ###
#yum install -y mysql-server mysql
service mysqld restart

#if [ -f /root/.provisioning/mysql_root_password ]; then
#    _old_pass=$(cat /root/.provisioning/mysql_root_password)
#    mysqladmin -u root --password=${_old_pass} password ${MYSQL_ROOT_PASSWORD}
#else
#    mysqladmin -u root password ${MYSQL_ROOT_PASSWORD}
#fi
#echo -n ${MYSQL_ROOT_PASSWORD} > /root/.provisioning/mysql_root_password

#if [ ${DROP_DB_IF_EXISTS} -eq 1 ]; then
#    for DB in ${DATABASES[@]}; do
#        echo "Dropping database if exists: ${DB}"
#        mysql -u root --password=${MYSQL_ROOT_PASSWORD} <<-DROPMSQL
#            DROP DATABASE IF EXISTS ${DB};
#        DROPMSQL
#    done
#fi

#for DB in ${DATABASES[@]}; do
#    echo "Create database if not exists: ${DB}"
#    mysql -u root --password=${MYSQL_ROOT_PASSWORD} <<-MSQL
#        CREATE DATABASE IF NOT EXISTS ${DB}; 
#    MSQL
#done

mysql -u root --password=${MYSQL_ROOT_PASSWORD} <<-MSQL
    GRANT ALL PRIVILEGES ON *.* TO root@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';
    FLUSH PRIVILEGES;
MSQL

### SMTP RELAY ###
#yum -y install postfix
#echo "relay_host = ${SMTP_RELAY}" >> /etc/postfix/main.cf 
#service postfix restart

## Drupal Specific Install
#echo "Getting composer..."

#curl -sS https://getcomposer.org/installer | php
#mv composer.phar /usr/local/bin/composer

#echo "Drupal/Site Specific Setup...."

#mkdir -p /vagrant/sites/default/files

#pushd /vagrant

#composer install
#echo "Setting up site..."
#./vendor/bin/drush -y site-install Standard --db-url="mysql://root:${MYSQL_ROOT_PASSWORD}@localhost/${DRUPAL_DATABASE}" --site-name=Example
#./vendor/bin/drush -y pm-enable cra
#./vendor/bin/drush -y vset theme_default cra

#popd