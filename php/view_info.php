<!DOCTYPE html>
<html>
<head>
    <title>View Information</title>
</head>
<body>
<?php
    error_reporting(E_ALL);
        if(isset($_POST['submit'])) {
            $user=mysql_real_escape_string($_POST['username']);
            $pass=sha1($_POST['password']);
            $first=mysql_real_escape_string($_POST['fname']);
            $mid=mysql_real_escape_string($_POST['minitial']);
            $last=mysql_real_escape_string($_POST['lname']);
            $email=test_input($_POST['email']);
            $address=mysql_real_escape_string($_POST['address']);
            $zc=$_POST['zipcode'];
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
             echo 'Invalid email address';
            }  else {
            ?>
                <p><b> User Name: </b><?=$user;?></p>
                <p><b> First Name: </b><?=$first;?> </p>
                <p><b> Middle Initial: </b><?=$mid;?> </p>
                <p><b> Last Name: </b><?=$last;?> </p>
                <p><b> Email Address: </b><?=$email;?> </p>
                <p><b> Address: </b><?=$address;?> </p>
                <p><b> Zip Code: </b><?=$zc;?></p>
        <?php
            }
            if (!file_exists('images/')) {
                mkdir('images/', 0777, true);
                $target_dir = "images/";
                $target_file = $target_dir . basename($_FILES["fileUp"]["name"]);
                $uploadOk = 1;
                $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                if(isset($_POST["submit"])) {
                        $check = getimagesize($_FILES["fileUp"]["tmp_name"]);
                        if($check !== false) {
                            echo "File is an image - " . $check["mime"] . ".";
                            $uploadOk = 1;
                        } else {
                            echo "File is not an image.";
                            $uploadOk = 0;
                        }
                }
                if (file_exists($target_file)) {
                    echo "Sorry, file already exists.";
                    $uploadOk = 0;
                }
                if ($_FILES["fileUp"]["size"] > 500000) {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                    echo "Sorry, only JPG, JPEG, PNG files are allowed.";
                    $uploadOk = 0;
                }
                if ($uploadOk == 0) {
                    echo "Sorry, your file was not uploaded.";
                } else {
                    if (move_uploaded_file($_FILES["fileUp"]["tmp_name"], $target_file)) {
                        echo "The file ". basename( $_FILES["fileUp"]["name"]). " has been uploaded.";
                    } else {
                        echo "Sorry, there was an error uploading your file.";
                    }
                } 
            }
        }        
        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        $user=filter_input(INPUT_POST, "username");
        $pass=sha1(filter_input(INPUT_POST,"password"));
        $first=filter_input(INPUT_POST,"fname");
        $mid=filter_input(INPUT_POST,"minitial");
        $last=filter_input(INPUT_POST,"lname");
        $email=filter_input(INPUT_POST,"email");
        $address=filter_input(INPUT_POST,"address");
        $zc=filter_input(INPUT_POST,"zipcode");
        $pic='images/'.basename($_FILES['fileUp']['name']);

        $output = $user . ",";
        $output .= $pass . ",";
        $output .= $first . ",";
        $output .= $mid . ",";
        $output .= $last . ",";
        $output .= $email . ",";
        $output .= $address . ",";
        $output .= $zc . ",";
        $output .= $pic . "\n";

        $fp = fopen("data.csv", "a");
        fwrite($fp, $output);
        fclose($fp);
?>
</body>
</html>
 